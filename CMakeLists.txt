cmake_minimum_required(VERSION 3.17)
project(SPO_lab1 C)

set(CMAKE_C_STANDARD 99)

add_executable(lab1 main.c hfsplus_utils.c hfs_plus_data_structures.c)